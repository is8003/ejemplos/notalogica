#include <iostream>

using namespace std;

int main(void){

	float n0, n1, n2, n3, n4;							// Declaro variables n0, n1, n2, n3, n4 de tipo flotante.

	cout << "Ingresa 5 notas (separadas por espacios): ";				// Pregunto al usuario 5 notas.
	cin >> n0 >> n1 >> n2 >> n3 >> n4;						// Obtengo y asigno valor de 5 notas.

	float prom = (n0 + n1 + n2 + n3 + n4) / 5;					// Declaro variable flotante prom y asigno promedio de 5 notas.
	cout << ((prom >= 3.0 && prom <= 5.0) ? "¡Pasaste!\n" : "No pasaste.\n");	// Si prom >= 3.0 && prom <= 5.0, entonces imprimo pasó asignatura, de lo contrario imprimo no pasó.

	return 0;
}
