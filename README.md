# ¿Pasé Introducción a la Programación?

Desarrolle un algoritmo que partiendo de cinco notas en el curso de
Introducción a la Programación, calcule su nota final como el promedio de esas
notas y genere un saludo que la nota final.

## Refinamiento 1

1. Preguntar cinco notas al usuario.
1. Calcular el promedio.
1. Verificar si pasó la asignatura.
1. Imprimir resultado.

## Refinamiento 2

1. Declaro variables `n0`, `n1`, `n2`, `n3`, `n4` de tipo flotante.
1. Pregunto al usuario 5 notas.
1. Obtengo y asigno valor de 5 notas.
1. Declaro variable flotante `prom` y asigno promedio de 5 notas.
1. Si `prom >= 3.0 && prom <= 5.0`, entonces imprimo pasó asignatura, de lo
   contrario imprimo no pasó.
